const { isUtf8 } = require("buffer");
const fs=require("fs");
const path=require("path");

function problem2(lipsum){
let lipsumpath=path.join(__dirname,lipsum);
fs.readFile(lipsumpath,"utf-8" ,(err,data) =>{
    if(err){
        console.error(err);
    }else{
        let filepath=path.join(__dirname,"uppercasefile.txt");
      
      fs.writeFile(filepath, data.toUpperCase(),(err,data) =>{
        if(err){
            console.error(err);
        }else{
            
            let file=path.basename(filepath);
            let filePath=path.join(__dirname,"filenames.txt");
            fs.writeFile(filePath,file,(err,data)=>{
                if(err){
                    console.error(err);
                }
            })

            fs.readFile(filepath,"utf-8",(err,uppercasefile)=>{
                if(err){
                    console.error(err);
                }else{
                    let lowercasepath=path.join(__dirname,"lowerCasefile.txt");
                    fs.writeFile(lowercasepath,uppercasefile.toLowerCase(),(err,data)=>{
                        if(err){
                            console.log(err);
                        }else{
                            let lowercasefilename=path.basename(lowercasepath);
                            fs.appendFile(filePath,`\n${lowercasefilename}`,(err,lowercasefile)=>{
                                if(err){
                                    console.error(err);
                                }else{
                                    let sortfile=path.join(__dirname,"sortFile.txt");
                                    
                                    fs.readFile(lowercasepath,"utf-8",(err,lowercasefiledata)=>{
                                        if(err){console.error(err);
                                        }else{
                                        
                                            let sortedarray=lowercasefiledata.split(".").sort().join("\n");
                                          
                                            fs.writeFile(sortfile,sortedarray,(err,data2)=>{
                                                if(err){
                                                    console.error(err);
                                                }else{
                                                    let sortedarrayfilename=path.basename(sortfile);
                                                    fs.appendFile(filePath,`\n${sortedarrayfilename}`,(err,filenamess)=>{
                                                        if(err){
                                                            console.error(err);
                                                        }else{

                                            fs.readFile(filePath,"utf-8",(err,filenamesdata)=>{
                                                let arr=filenamesdata.split("\n");
                                            
                                              
                                                if(err){
                                                    console.error(err);
                                                }
                                            else{
                                                for(let val=0;val<arr.length;val++){
                                                    let eachfilepath=path.join(__dirname,arr[val]);
                                                fs.unlink(eachfilepath,(err,data)=>{
                                                    if(err){
                                                        console.error(err);
                                                    }
                                                })
                                                }
                                            }
                                            })
                                                        }
                                                    })
                                                }
                                            })
                                          


                                        
                                        }
                                    })
                                 
                                }
                            })
                        }
                    });
                }
            })
           

        }


      })
    }
})

return "successfully completed";
}
module.exports=problem2;
