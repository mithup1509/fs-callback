const fs = require("fs");
const path = require("path");

function workwithCallback(filename, count, cb) {
  let filepath = path.join(__dirname, filename);
  fs.mkdir(filepath, (err, data) => {
    if (err) {
      cb(err);
    } else {
      for (let val = 1; val <= count; val++) {
        let filespath = path.join(__dirname, filename, `file${val}.json`);

        fs.writeFile(filespath, "hello miithup", (err, data) => {
          if (err) {
            cb(err);
          } else {
            console.log(`file${val}.json created successfully`);
            fs.unlink(filespath, (err, data) => {
              if (err) {
                console.error(err);
              } else {
                console.log(`file${val}.json deleted successfully`);
              }
            });
          }
        });
      }
    }
  });
}

module.exports = workwithCallback;
